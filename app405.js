//Spread: Merg/Concat
const a1 = [1,2,3];
const a2 = [4,5,6];
const a3 = [7,8,9];
const user1 = {name : "Mark Zuckerberg"};
const user2 = {name : "Elon Mark"};
const arr = [...a1, ...a2, ...a3];
console.log(arr)

const users = {...user1, ...user2}
const {usera,userb} = users;
console.log(usera)